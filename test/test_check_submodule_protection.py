from textwrap import dedent
from unittest.mock import patch, MagicMock

import pytest

from ci_tools import check_submodule_protection as csp
from ci_tools.check_submodule_protection import _colored


def test_run_command():
    with patch('subprocess.check_output') as check_output:
        check_output.return_value = b'test output\n'
        result = csp.run_command('ls')
        assert result == 'test output'


@pytest.mark.parametrize(('url', 'owner', 'repo'), [
    ('git@gitlab.com:janoskut/ci-tools.git', 'janoskut', 'ci-tools'),
    ('https://gitlab.com/janoskut/ci-tools.git', 'janoskut', 'ci-tools'),
    ('https://github.com/python/cpython', 'python', 'cpython'),
    ('git@github.com:python/cpython.git', 'python', 'cpython'),
])
def test_submodule_init(url: str, owner: str, repo: str) -> None:

    with patch('subprocess.check_output') as check_output:
        check_output.return_value = url.encode('utf-8')
        submodule = csp.Submodule('path', 'commit', 'describe')

    assert submodule.path == 'path'
    assert submodule.commit == 'commit'
    assert submodule.describe == 'describe'
    assert submodule.url == url
    assert submodule.owner == owner
    assert submodule.repo == repo

    assert submodule.__repr__() == 'path commit describe'


@patch.object(csp.Submodule, '_resolve_remote')
def test_submodule_get_branches(mock_resolve_remote: MagicMock) -> None:
    mock_resolve_remote.return_value = ('url', 'owner', 'repo')

    with patch('subprocess.check_output') as check_output:
        check_output.return_value = b"""
        branch1
        branch2
        branch2
        remotes/origin/branch3

        (HEAD detached at 77bb185)
        remotes/origin/HEAD -> origin/main
        """
        submodule = csp.Submodule('path', 'commit', 'describe')
        branches = submodule.get_branches()
        assert sorted(branches) == sorted(['branch1', 'branch2', 'branch3'])


@pytest.mark.parametrize('url', [
    'https://unsupported.com/owner/repo.git',
    'https://other.com/owner/repo.git',
])
@patch.object(csp.Submodule, '_resolve_remote')
def test_get_protected_branches_unsupported_hoster(_resolve_remote: MagicMock, url: str) -> None:
    _resolve_remote.return_value = (url, 'owner', 'repo')

    with pytest.raises(ValueError):
        submodule = csp.Submodule('path', 'commit', 'describe')
        submodule.get_remotes_protected_branches()


@pytest.mark.parametrize(('url', 'token'), [
    ('https://github.com/owner/repo', 'GITHUB_TOKEN'),
    ('https://gitlab.com/owner/repo.git', 'GITLAB_TOKEN'),
])
@patch.object(csp.Submodule, '_resolve_remote')
def test_get_protected_branches_no_token(_resolve_remote: MagicMock, url: str, token: str) -> None:
    _resolve_remote.return_value = (url, 'owner', 'repo')
    with patch.dict('os.environ', {token: ''}):
        with pytest.raises(csp.MissingTokenError):
            submodule = csp.Submodule('path', 'commit', 'describe')
            submodule.get_remotes_protected_branches()


@pytest.mark.parametrize('url', [
    'https://gitlab.com/owner/repo.git',
    'https://github.com/owner/repo.git',
])
@pytest.mark.parametrize('status_code', [201, 404, 500, 0, -1, 123456789])
@patch.dict('os.environ', {'GITLAB_TOKEN': 'GITLAB_TOKEN', 'GITHUB_TOKEN': 'GITHUB_TOKEN'})
@patch.object(csp.Submodule, '_resolve_remote')
def test_get_gitlab_protected_branches_request_error(_resolve_remote: MagicMock, url: str, status_code: int) -> None:
    _resolve_remote.return_value = (url, 'owner', 'repo')
    with patch('requests.get') as requests_get:
        requests_get.return_value = MagicMock(status_code=status_code)
        submodule = csp.Submodule('path', 'commit', 'describe')
        with pytest.raises(RuntimeError):
            submodule.get_remotes_protected_branches()


@pytest.mark.parametrize('url', [
    'https://github.com/owner/repo',
])
@patch.dict('os.environ', {'GITHUB_TOKEN': 'GITHUB_TOKEN'})
@patch.object(csp.Submodule, '_resolve_remote')
def test_get_github_protected_branches(_resolve_remote: MagicMock, url: str) -> None:
    _resolve_remote.return_value = (url, 'owner', 'repo')
    with patch('requests.get') as requests_get:
        mock_response = MagicMock()
        mock_response.status_code = 200
        mock_response.text = '[{"name": "branch1", "protected": true}, {"name": "branch2", "protected": false}]'
        requests_get.return_value = mock_response
        submodule = csp.Submodule('path', 'commit', 'describe')
        branches = submodule.get_remotes_protected_branches()
        assert branches == ['branch1']
        requests_get.assert_called_once_with(
            "https://api.github.com/repos/owner/repo/branches",
            headers={
                'Authorization': "token GITHUB_TOKEN",
                'Accept': "application/vnd.github.v3+json",
            },
        )


@pytest.mark.parametrize('url', [
    'https://gitlab.com/owner/repo.git',
])
@patch.dict('os.environ', {'GITLAB_TOKEN': 'GITLAB_TOKEN'})
@patch.object(csp.Submodule, '_resolve_remote')
def test_get_gitlab_protected_branches(_resolve_remote: MagicMock, url: str) -> None:
    _resolve_remote.return_value = (url, 'owner', 'repo')
    with patch('requests.get') as requests_get:
        mock_response = MagicMock()
        mock_response.status_code = 200
        mock_response.text = '[{"name": "branch1"}, {"name": "branch2"}]'
        requests_get.return_value = mock_response
        submodule = csp.Submodule('path', 'commit', 'describe')
        branches = submodule.get_remotes_protected_branches()
        assert sorted(branches) == sorted(['branch1', 'branch2'])
        requests_get.assert_called_once_with(
            "https://gitlab.com/api/v4/projects/owner%2Frepo/protected_branches",
            headers={
                'Authorization': "Bearer GITLAB_TOKEN",
                'Accept': "application/json",
            },
        )


@patch('ci_tools.check_submodule_protection.run_command')
@patch('ci_tools.check_submodule_protection.Submodule._resolve_remote')
def test_get_submodules_non_recursive(_resolve_remote: MagicMock, run_command: MagicMock) -> None:
    run_command.side_effect = [
        """
        +commit1 sm_1 (describe1)
        -commit2 sub/sm_2 (describe2)
        \n
        commit3 sub/foo/sm_3 foo
        """  # git submodule status
    ]
    _resolve_remote.side_effect = [
        ('url_1', 'owner_1', 'repo_1'),  # git remote get-url origin (sm_1)
        ('url_2', 'owner_2', 'repo_2'),  # git remote get-url origin (sub/sm_2)
        ('url_3', 'owner_3', 'repo_3'),  # git remote get-url origin (sub/foo/sm_3)
    ]
    expected_results = [
        {
            'commit': 'commit1',
            'path': 'root/sm_1',
            'describe': '(describe1)',
            'url': 'url_1',
            'owner': 'owner_1',
            'repo': 'repo_1',
        },
        {
            'commit': 'commit2',
            'path': 'root/sub/sm_2',
            'describe': '(describe2)',
            'url': 'url_2',
            'owner': 'owner_2',
            'repo': 'repo_2',
        },
        {
            'commit': 'commit3',
            'path': 'root/sub/foo/sm_3',
            'describe': 'foo',
            'url': 'url_3',
            'owner': 'owner_3',
            'repo': 'repo_3',
        },
    ]

    submodules = csp.get_submodules('root')

    assert len(submodules) == len(expected_results)

    for submodule, expected in zip(submodules, expected_results):
        assert submodule.commit == expected['commit']
        assert submodule.path == expected['path']
        assert submodule.describe == expected['describe']
        assert submodule.url == expected['url']
        assert submodule.owner == expected['owner']
        assert submodule.repo == expected['repo']


@patch('ci_tools.check_submodule_protection.run_command')
@patch('ci_tools.check_submodule_protection.Submodule._resolve_remote')
def test_get_submodules_recursive(_resolve_remote: MagicMock, run_command: MagicMock) -> None:
    """Testing the following recursive submodule structure.

    Logical structure:
        root
        ├── sm_1
        └── sm_2
            └── sm_3
                ├── sm_4
                └── sm_5

    Directory structure:
        root
        ├── sm_1
        └── sub
            └── sm_2
                └── sm_3
                    ├── sm_4
                    └── sub
                        └── sm_5

    Results:
        root/sm_1
        root/sub/sm_2
        root/sub/sm_2/sm_3
        root/sub/sm_2/sm_3/sm_4
        root/sub/sm_2/sm_3/sub/sm_5
    """
    run_command.side_effect = [
        # git submodule status (root)
        "commit1 sm_1 (describe1)\ncommit2 sub/sm_2 (describe2)",
        # git submodule status (sm_1) -> no recursive submodule
        "",
        # git submodule status (sm_2) -> 1 recursive submodule
        "commit3 sm_3 (describe3)",
        # git submodule status (sm_2/sm_3) -> 2 recursive submodules
        "commit4 sm_4 (describe4)\ncommit5 sub/sm_5 (describe5)",
        # git submodule status (sm_2/sm_3/sm_4) -> no recursive submodule
        "",
        # git submodule status (sm_2/sm_3/m_5) -> no recursive submodule
        "",
    ]
    _resolve_remote.return_value = ('url', 'owner', 'repo')  # git remote get-url origin (all submodules)
    expected_results = [
        {
            'commit': 'commit1',
            'path': 'root/sm_1',
            'describe': '(describe1)',
        },
        {
            'commit': 'commit2',
            'path': 'root/sub/sm_2',
            'describe': '(describe2)',
        },
        {
            'commit': 'commit3',
            'path': 'root/sub/sm_2/sm_3',
            'describe': '(describe3)',
        },
        {
            'commit': 'commit4',
            'path': 'root/sub/sm_2/sm_3/sm_4',
            'describe': '(describe4)',
        },
        {
            'commit': 'commit5',
            'path': 'root/sub/sm_2/sm_3/sub/sm_5',
            'describe': '(describe5)',
        },
    ]

    submodules = csp.get_submodules('root', recursive=True)

    assert len(submodules) == len(expected_results)

    for submodule, expected in zip(submodules, expected_results):
        assert submodule.commit == expected['commit']
        assert submodule.path == expected['path']
        assert submodule.describe == expected['describe']
        assert submodule.url == 'url'
        assert submodule.owner == 'owner'
        assert submodule.repo == 'repo'


@pytest.mark.parametrize('is_tty', [True, False])
@pytest.mark.parametrize('color', [True, False])
@pytest.mark.parametrize('show', [True, False])
@patch('ci_tools.check_submodule_protection.sys.stdout.isatty')
@patch('ci_tools.check_submodule_protection.get_submodules')
def test_check_submodule_protection_all_protected(
    get_submodules: MagicMock,
    isatty_mock: MagicMock,
    show: bool,
    color: bool,
    is_tty: bool,
    capsys
) -> None:

    isatty_mock.return_value = is_tty

    sm_1 = MagicMock()
    sm_1.path = 'root/path1'
    sm_1.commit = 'commit1'
    sm_1.describe = 'describe1'
    sm_1.get_branches.return_value = ['branch1']
    sm_1.get_remotes_protected_branches.return_value = ['branch1', 'branch2']

    sm_2 = MagicMock()
    sm_2.path = 'root/path2'
    sm_2.commit = 'commit2'
    sm_2.describe = 'describe2'
    sm_2.get_branches.return_value = ['branch3', 'branch4']
    sm_2.get_remotes_protected_branches.return_value = ['branch4', 'branch5']

    get_submodules.return_value = [sm_1, sm_2]

    result = csp.check_submodule_protection('root', show=show, recursive=True, no_color=not color)

    assert result is True

    captured = capsys.readouterr()

    if show:
        if color and is_tty:
            assert captured.out.strip() == dedent(
                f"""
                    {_colored('path1', 'green')} commit1 describe1
                        branch1 {_colored('(protected)', 'green')}
                    {_colored('path2', 'green')} commit2 describe2
                        branch3
                        branch4 {_colored('(protected)', 'green')}
                """
            ).strip()
        else:
            assert captured.out.strip() == dedent(
                """
                    path1 commit1 describe1
                        branch1 (protected)
                    path2 commit2 describe2
                        branch3
                        branch4 (protected)
                """
            ).strip()
    else:
        assert captured.out == ''


@pytest.mark.parametrize('is_tty', [True, False])
@pytest.mark.parametrize('color', [True, False])
@pytest.mark.parametrize('show', [True, False])
@pytest.mark.parametrize('protected_branches', [
    [],
    ['branch2'],
    ['branch2', 'branch3'],
])
@patch('ci_tools.check_submodule_protection.sys.stdout.isatty')
@patch('ci_tools.check_submodule_protection.get_submodules')
def test_check_submodule_protection_no_protected_branches(
    get_submodules: MagicMock,
    isatty_mock: MagicMock,
    protected_branches: list[str],
    show: bool,
    color: bool,
    is_tty: bool,
    capsys
) -> None:

    isatty_mock.return_value = is_tty

    sm_1 = MagicMock()
    sm_1.path = 'root/path1'
    sm_1.commit = 'commit1'
    sm_1.describe = 'describe1'
    sm_1.get_branches.return_value = ['branch1']
    sm_1.get_remotes_protected_branches.return_value = protected_branches

    sm_2 = MagicMock()
    sm_2.path = 'root/path2'
    sm_2.commit = 'commit2'
    sm_2.describe = 'describe2'
    sm_2.get_branches.return_value = ['branch3', 'branch4']
    sm_2.get_remotes_protected_branches.return_value = ['branch3', 'branch4']

    sm_3 = MagicMock()
    sm_3.path = 'root/path3'
    sm_3.commit = 'commit3'
    sm_3.describe = 'describe3'
    sm_3.get_branches.return_value = []
    sm_3.get_remotes_protected_branches.return_value = ['branchA', 'branchB']

    get_submodules.return_value = [sm_1, sm_2, sm_3]

    result = csp.check_submodule_protection('root', show=show, recursive=False, no_color=not color)

    if show:
        assert result is True
    else:
        assert result is False

    captured = capsys.readouterr()

    if show:
        if color and is_tty:
            assert captured.out.strip() == dedent(
                f"""
                    {_colored('path1', 'red')} commit1 describe1
                        branch1
                    {_colored('path2', 'green')} commit2 describe2
                        branch3 {_colored('(protected)', 'green')}
                        branch4 {_colored('(protected)', 'green')}
                    {_colored('path3', 'red')} commit3 describe3
                        {_colored('<no branches>', 'yellow')}
                """
            ).strip()
        else:
            assert captured.out.strip() == dedent(
                """
                    path1 commit1 describe1
                        branch1
                    path2 commit2 describe2
                        branch3 (protected)
                        branch4 (protected)
                    path3 commit3 describe3
                        <no branches>
                """
            ).strip()
    else:
        assert captured.err.strip() == dedent(
            """
                ERROR: Commit commit1 in path1 is not in a protected branch.
                ERROR: Commit commit3 in path3 is not in a protected branch.
            """
        ).strip()


@pytest.mark.parametrize('is_tty', [True, False])
@pytest.mark.parametrize('color', [True, False])
@pytest.mark.parametrize('show', [True, False])
@patch('ci_tools.check_submodule_protection.sys.stdout.isatty')
@patch('ci_tools.check_submodule_protection.get_submodules')
def test_check_submodule_protection_runtime_error(
    get_submodules: MagicMock,
    isatty_mock: MagicMock,
    show: bool,
    color: bool,
    is_tty: bool,
    capsys
) -> None:

    isatty_mock.return_value = is_tty

    sm_1 = MagicMock()
    sm_1.path = 'root/path1'
    sm_1.commit = 'commit1'
    sm_1.describe = 'describe1'
    sm_1.get_branches.return_value = ['branch1']
    sm_1.get_remotes_protected_branches.return_value = ['branch1', 'branch2']

    sm_2 = MagicMock()
    sm_2.path = 'root/path2'
    sm_2.commit = 'commit2'
    sm_2.describe = 'describe2'
    sm_2.get_branches.return_value = ['branch3', 'branch4']
    sm_2.get_remotes_protected_branches.return_value = ['branch3', 'branch4']

    sm_3 = MagicMock()
    sm_3.path = 'root/path3'
    sm_3.commit = 'commit3'
    sm_3.describe = 'describe3'
    sm_3.get_branches.return_value = ['branch4', 'branch5', 'branch6']
    sm_3.get_remotes_protected_branches.side_effect = RuntimeError("Failed to retrieve protected branches")

    get_submodules.return_value = [sm_1, sm_2, sm_3]

    result = csp.check_submodule_protection('root', show=show, recursive=False, no_color=not color)

    if show:
        assert result is True
    else:
        assert result is False

    captured = capsys.readouterr()

    print(captured.out)

    if show:
        if color and is_tty:
            assert captured.out.strip() == dedent(
                f"""
                    {_colored('path1', 'green')} commit1 describe1
                        branch1 {_colored('(protected)', 'green')}
                    {_colored('path2', 'green')} commit2 describe2
                        branch3 {_colored('(protected)', 'green')}
                        branch4 {_colored('(protected)', 'green')}
                    {_colored('path3', 'red')} commit3 describe3
                        {_colored('<failed to retrieve protected branches info from API.>', 'red')}
                        branch4
                        branch5
                        branch6
                """
            ).strip()
        else:
            assert captured.out.strip() == dedent(
                """
                    path1 commit1 describe1
                        branch1 (protected)
                    path2 commit2 describe2
                        branch3 (protected)
                        branch4 (protected)
                    path3 commit3 describe3
                        <failed to retrieve protected branches info from API.>
                        branch4
                        branch5
                        branch6
                """
            ).strip()
    else:
        assert captured.err.strip() == dedent(
            """
                ERROR: Commit commit3 in path3 is not in a protected branch.
            """
        ).strip()
