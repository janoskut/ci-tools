# CI Tools

[![pipeline status](https://gitlab.com/janoskut/ci-tools/badges/main/pipeline.svg)](https://gitlab.com/janoskut/ci-tools/pipelines/main/latest)
[![coverage](https://gitlab.com/janoskut/ci-tools/badges/main/coverage.svg?job=coverage&key_text=coverage)](https://gitlab.com/janoskut/ci-tools/-/jobs/artifacts/main/file/htmlcov/index.html?job=coverage)

A collection of CI utilities.

## Check Submodules Branch Protection States

For stable release cycles it is not only important to release from protected branches and tags, but also that submodule references point to protected commits. Otherwise, submodule history could be re-written, which would break the released state of the using repository.

The script `check_submodule_protection.py` recursively checks submodules and determines branch protection states, using the GitHub and GitLab API. Required is to have corresponding API tokens available in the environment (`GITHUB_TOKEN`, `GITLAB_TOKEN`).
