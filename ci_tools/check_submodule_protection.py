"""
Check submodule branch protection for a GIT repository.

This script checks if the commits of all submodules in a GIT repository are in a protected branch.
The protection status of branches is determined by the GitHub API. (GitHub is the only supported hoster at the moment.)

For using the GitHub API, the environment variable GITHUB_TOKEN must be set to a valid GitHub access token.

The program exits with code 0 if all submodules are in a protected branch, otherwise with code 1.

Usage:
    check-protected-submodules.py [options] [path]
"""

import argparse
import json
import logging
import os
import subprocess
import sys

import requests


logging.basicConfig(level=logging.INFO, format="%(asctime)s %(levelname)8s %(message)s")
log = logging.getLogger()


class MissingTokenError(Exception):
    """Raised when the token environment variable is not set."""
    pass


def run_command(command: str) -> str:
    log.debug(f"> cmd: {command}")
    return subprocess.check_output(command, shell=True).decode('utf-8').strip()


def shell_supports_colors() -> bool:
    return sys.stdout.isatty()


def colored(text: str, color: str) -> str:
    if not shell_supports_colors():
        return text
    return _colored(text, color)


def _colored(text: str, color: str) -> str:
    """Return colored text for the terminal.

    Args:
        text: The text to color.
        color: The color to use. One of 'red', 'green', 'yellow', 'blue', 'magenta', 'cyan', 'white'.

    Returns:
        The colored text.
    """
    color_code = {
        'red': 91,
        'green': 92,
        'yellow': 93,
        'blue': 94,
        'magenta': 95,
        'cyan': 96,
        'white': 97,
    }
    assert color in color_code, f"Invalid color: {color}"
    return f"\033[{color_code[color]}m{text}\033[0m"


class Submodule:

    def __init__(self, path: str, commit: str, describe: str):
        self.path = path
        self.commit = commit
        self.describe = describe
        url, owner, repo = self._resolve_remote()
        self.url = url
        self.owner = owner
        self.repo = repo

    def __repr__(self) -> str:
        return f"{self.path} {self.commit} {self.describe}"

    def get_branches(self) -> list[str]:
        result = run_command(f"git -C {self.path} branch -a --contains {self.commit}")
        branches = result.split('\n') if result else []
        filtered = {
            branch.replace("remotes/origin/", "").strip() for branch in branches if branch and "HEAD" not in branch
        }
        return list(filtered)

    def _resolve_remote(self) -> tuple[str, str, str]:
        url = run_command(f"git -C {self.path} remote get-url origin")
        if "@" in url and ":" in url:
            # SSH URL
            owner, repo = url.split(":")[-1].split("/")
        else:
            # HTTP URL
            owner, repo = url.split("/")[-2:]
        repo = repo.replace(".git", "")
        return url, owner, repo

    def get_remotes_protected_branches(self) -> list[str]:
        """Get protected branches from hoster API.

        Supported hosters:
        - github.com
        - gitlab.com

        Raises:
            MissingTokenError: If the token environment variable is not set.
            RuntimeError: If the API request fails.
            ValueError: If the remote URL is invalid.
        """
        if 'github.com' in self.url:
            service = 'github'
            url = f"https://api.github.com/repos/{self.owner}/{self.repo}/branches"
            token = os.environ.get('GITHUB_TOKEN')
            token_header = 'token'
            accept_header = "application/vnd.github.v3+json"
        elif 'gitlab.com' in self.url:
            service = 'gitlab'
            url = f"https://gitlab.com/api/v4/projects/{self.owner}%2F{self.repo}/protected_branches"
            token = os.environ.get('GITLAB_TOKEN')
            token_header = 'Bearer'
            accept_header = "application/json"
        else:
            raise ValueError("Invalid remote URL. Only github.com and gitlab.com are supported.")

        if not token:
            raise MissingTokenError(f"ERROR: {service.upper()}_TOKEN environment variable is not set")

        headers = {
            'Authorization': f"{token_header} {token}",
            'Accept': accept_header,
        }

        response = requests.get(url, headers=headers)
        if response.status_code != 200:
            raise RuntimeError(
                f"ERROR: Could not get protected branches from {service} API: {response.text} "
                f"(status code: {response.status_code})"
            )

        branches = json.loads(response.text)
        if service == 'github':
            return [branch['name'] for branch in branches if branch['protected']]
        elif service == 'gitlab':
            return [branch['name'] for branch in branches]
        assert False, "This should never happen"  # pragma: no cover


def get_submodules(path: str = '.', recursive: bool = False) -> list[Submodule]:
    sm_status = run_command(f"git -C {path} submodule status")
    sm_status_lines = sm_status.strip().split('\n') if sm_status else []
    submodules: list[Submodule] = []
    for line in sm_status_lines:
        line = line.strip()
        if not line:
            continue
        commit, sm_path, describe = line.split(' ')
        commit = commit.lstrip('-+')
        submodules.append(Submodule(path=f"{path}/{sm_path}", commit=commit, describe=describe))

    recursive_submodules: list[Submodule] = []
    for submodule in submodules:
        log.debug(f"Submodule: {submodule}")
        if recursive:
            recursive_submodules.extend(get_submodules(submodule.path, recursive=recursive))
    return submodules + recursive_submodules


def check_submodule_protection(path: str, show: bool, recursive: bool, no_color: bool = False) -> bool:
    submodules = get_submodules(path, recursive=recursive)
    success = True

    _colored = colored if not no_color else lambda text, color: text

    for submodule in submodules:
        protected_branches = []
        remote_error = False
        try:
            protected_branches = submodule.get_remotes_protected_branches()
        except RuntimeError as exc:
            log.warning(exc)
            remote_error = True

        submodule_branches = submodule.get_branches()
        protected_submodule_branches = set(protected_branches) & set(submodule_branches)

        rel_path = os.path.relpath(submodule.path, path)

        if show:
            protected = bool(set(protected_branches) & set(submodule_branches))
            sm_path = _colored(rel_path, "green" if protected else "red")

            print(f"{sm_path} {submodule.commit} {submodule.describe}")
            if remote_error:
                print(f"    {_colored('<failed to retrieve protected branches info from API.>', 'red')}")
            for branch in submodule_branches:
                protected_str = _colored("(protected)", 'green') if branch in protected_branches else ""
                print(f"    {branch} {protected_str}".rstrip())
            if not submodule_branches:
                print(f"    {_colored('<no branches>', 'yellow')}")

        else:
            if not protected_submodule_branches:
                print(
                    f"ERROR: Commit {submodule.commit} in {rel_path} is not in a protected branch.",
                    file=sys.stderr
                )
                success = False
    return success


def main():  # pragma: no cover
    parser = argparse.ArgumentParser(description=__doc__, formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('repository_path', metavar='path', nargs='?', default='.', help="Path to the GIT repository")
    parser.add_argument('-d', '--debug', action='store_true', help="Enable debug logging")
    parser.add_argument('-s', '--show', action='store_true', help="Show submodule branches")
    parser.add_argument('-r', '--recursive', action='store_true', help="Check submodules recursively")
    parser.add_argument('--no-color', action='store_true', help="Disable colored output")
    args = parser.parse_args()

    if args.debug:
        log.setLevel(logging.DEBUG)

    success = check_submodule_protection(
        path=args.repository_path, show=args.show, recursive=args.recursive, no_color=args.no_color
    )

    exit(0 if success is True else 1)


if __name__ == '__main__':  # pragma: no cover
    main()
